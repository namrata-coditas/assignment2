import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PastEventListDetailsComponent } from './past-event-list-details.component';

describe('PastEventListDetailsComponent', () => {
  let component: PastEventListDetailsComponent;
  let fixture: ComponentFixture<PastEventListDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PastEventListDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PastEventListDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
