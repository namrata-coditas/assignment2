import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AncientWarriorsComponent } from './ancient-warriors.component';

describe('AncientWarriorsComponent', () => {
  let component: AncientWarriorsComponent;
  let fixture: ComponentFixture<AncientWarriorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AncientWarriorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AncientWarriorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
