import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AncientWarriorsEventButtonsComponent } from './ancient-warriors-event-buttons.component';

describe('AncientWarriorsEventButtonsComponent', () => {
  let component: AncientWarriorsEventButtonsComponent;
  let fixture: ComponentFixture<AncientWarriorsEventButtonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AncientWarriorsEventButtonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AncientWarriorsEventButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
