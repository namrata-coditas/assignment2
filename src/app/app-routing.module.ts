import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignupComponent } from './signup/signup.component';
import { SignUpWithSportsExchangeComponent } from './sign-up-with-sports-exchange/sign-up-with-sports-exchange.component';
import { PasswordRecoverComponent } from './password-recover/password-recover.component';
import { AncientWarriorsComponent } from './ancient-warriors/ancient-warriors.component';
import { PastEventListComponent } from './past-event-list/past-event-list.component';


const routes: Routes = [
  {path:'assignment1',component:SignupComponent},
  {path:'assignment2',component:SignUpWithSportsExchangeComponent},
  {path:'assignment3',component:PasswordRecoverComponent},
  {path:'assignment4',component:AncientWarriorsComponent},
  {path:'assignment5',component:PastEventListComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
