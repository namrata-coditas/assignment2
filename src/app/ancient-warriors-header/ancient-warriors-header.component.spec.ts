import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AncientWarriorsHeaderComponent } from './ancient-warriors-header.component';

describe('AncientWarriorsHeaderComponent', () => {
  let component: AncientWarriorsHeaderComponent;
  let fixture: ComponentFixture<AncientWarriorsHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AncientWarriorsHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AncientWarriorsHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
