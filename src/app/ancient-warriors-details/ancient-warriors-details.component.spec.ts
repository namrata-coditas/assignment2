import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AncientWarriorsDetailsComponent } from './ancient-warriors-details.component';

describe('AncientWarriorsDetailsComponent', () => {
  let component: AncientWarriorsDetailsComponent;
  let fixture: ComponentFixture<AncientWarriorsDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AncientWarriorsDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AncientWarriorsDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
