import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignUpWithSportsExchangeComponent } from './sign-up-with-sports-exchange.component';

describe('SignUpWithSportsExchangeComponent', () => {
  let component: SignUpWithSportsExchangeComponent;
  let fixture: ComponentFixture<SignUpWithSportsExchangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignUpWithSportsExchangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignUpWithSportsExchangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
