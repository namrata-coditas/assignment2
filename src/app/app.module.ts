import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SignupComponent } from './signup/signup.component';
import { SignUpWithSportsExchangeComponent } from './sign-up-with-sports-exchange/sign-up-with-sports-exchange.component';
import { PasswordRecoverComponent } from './password-recover/password-recover.component';
import { HeaderComponent } from './header/header.component';
import { PracticeComponent } from './practice/practice.component';
import { RecoverPasswordComponent } from './recover-password/recover-password.component';
import { AncientWarriorsComponent } from './ancient-warriors/ancient-warriors.component';
import { AncientWarriorsHeaderComponent } from './ancient-warriors-header/ancient-warriors-header.component';
import { AncientWarriorsDetailsComponent } from './ancient-warriors-details/ancient-warriors-details.component';
import { PastEventListComponent } from './past-event-list/past-event-list.component';
import { AncientWarriorsProfileHeaderComponent } from './ancient-warriors-profile-header/ancient-warriors-profile-header.component';
import { AncientWarriorsEventButtonsComponent } from './ancient-warriors-event-buttons/ancient-warriors-event-buttons.component';
import { PastEventListDetailsComponent } from './past-event-list-details/past-event-list-details.component';
import { RosterListComponent } from './roster-list/roster-list.component';

@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    SignUpWithSportsExchangeComponent,
    PasswordRecoverComponent,
    HeaderComponent,
    PracticeComponent,
    RecoverPasswordComponent,
    AncientWarriorsComponent,
    AncientWarriorsHeaderComponent,
    AncientWarriorsDetailsComponent,
    PastEventListComponent,
    AncientWarriorsProfileHeaderComponent,
    AncientWarriorsEventButtonsComponent,
    PastEventListDetailsComponent,
    RosterListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
