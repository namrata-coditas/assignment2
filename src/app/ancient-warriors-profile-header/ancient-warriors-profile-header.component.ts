import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ancient-warriors-profile-header',
  templateUrl: './ancient-warriors-profile-header.component.html',
  styleUrls: ['./ancient-warriors-profile-header.component.css']
})
export class AncientWarriorsProfileHeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
