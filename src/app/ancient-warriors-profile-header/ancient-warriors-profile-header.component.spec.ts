import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AncientWarriorsProfileHeaderComponent } from './ancient-warriors-profile-header.component';

describe('AncientWarriorsProfileHeaderComponent', () => {
  let component: AncientWarriorsProfileHeaderComponent;
  let fixture: ComponentFixture<AncientWarriorsProfileHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AncientWarriorsProfileHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AncientWarriorsProfileHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
